package controller;

import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();
	
	public static void loadRoutes() {
		
	}
		
	public static void loadTrips() {
		
	}

	public static void loadStopTimes() {
		
	}
	
	public static void loadStops() {
		
	}
	
	public static IList<VORoute> routeAtStop(String stopName) {
		return null;
	}
	
	public static IList<VOStop> stopsRoute(String routeName, String direction) {
		return null;
	}
}
